class ApplicationController < ActionController::Base
  helper :all
  protect_from_forgery with: :exception

  def connected_user
    connected_user=User.all[0]
  end

end
